//
//  Home.swift
//  ARKitExample
//
//  Created by Kirill Ryabinin on 08.11.2017.
//  Copyright © 2017 Apple. All rights reserved.
//

import Foundation

class Home: VirtualObject {
	
	override init() {
		super.init(modelName: "home", fileExtension: "scn", thumbImageFilename: "home", title: "Home")
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
